﻿using System;
using System.Net.Http;
using BannerFlowApi.Data;
using BannerFlowApi.Test.Fakes;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using BannerFlowApi.Test.Seed;

namespace BannerFlowApi.Test.Host
{
    public class TestHost
    {
        public HttpClient HttpClient { get; }
        public DbSeeder Seed { get; }

        public TestHost(HostServiceType hostServiceType = HostServiceType.Fake)
        {
            IWebHostBuilder webHost;

            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.Test.json")
                .Build();
            
            switch (hostServiceType)
            {
                case HostServiceType.Fake:
                   webHost = new WebHostBuilder()
                        .ConfigureTestServices(service =>
                            service.AddSingleton<IBannerRepository, FakeBannerRepository>())
                        .UseStartup<Startup>();
                    break;
                case HostServiceType.Database:
                    webHost = new WebHostBuilder()
                        .UseConfiguration(config)
                        .UseStartup<Startup>();
                    var mongoDbSettings = config.GetSection(nameof(MongoDbSettings)).Get<MongoDbSettings>();

                    Seed = new DbSeeder(mongoDbSettings);

                    Seed.Seed();
                    break;
                default:
                    throw new ArgumentException(nameof(HostServiceType));

            }

            var testserver = new TestServer(webHost);

            HttpClient = testserver.CreateClient();
        }
    }

    public enum HostServiceType
    {
        Fake,
        Database
    }
}