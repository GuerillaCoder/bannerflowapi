﻿using System;
using System.Collections.Generic;
using BannerFlowApi.DomainModels;
using MongoDB.Driver;

namespace BannerFlowApi.Test.Seed
{
    public class DbSeeder
    {
        private readonly IMongoCollection<Banner> _bannerCollection;
        public DbSeeder(MongoDbSettings mongoDbSettings)
        {
            var client = new MongoClient(mongoDbSettings.ConnectionString);
            var db = client.GetDatabase(mongoDbSettings.DatabaseName);

            _bannerCollection = db.GetCollection<Banner>(mongoDbSettings.CollectionName);
        }

        public void Seed()
        {
            var banners = new List<Banner>
            {
                new Banner("5b1d3c203e10bd6604d6c8d6", "<div></div>", DateTime.Now, null),
                new Banner("5b1d3ba13e10bd6604d6c8d4", "<div><p></p></div>", DateTime.Now.AddDays(-4), DateTime.Now.AddDays(-1))
            };

            _bannerCollection.InsertMany(banners);
        }

        public void UnSeed()
        {
            _bannerCollection.DeleteMany(_ => true);
        }
    }
}
