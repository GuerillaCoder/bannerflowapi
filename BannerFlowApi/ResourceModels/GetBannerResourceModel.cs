﻿using System;
using BannerFlowApi.DomainModels;

namespace BannerFlowApi.ResourceModels
{
    public class GetBannerResourceModel
    {
        public string Id { get; set; }
        public string Html { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }

        public static GetBannerResourceModel FromBanner(Banner banner)
        {
            return new GetBannerResourceModel
            {
                Created = banner.Created,
                Html = banner.Html,
                Id = banner.Id,
                Modified = banner.Modified
            };
        }
    }
}
