﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using BannerFlowApi.DomainModels;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace BannerFlowApi.Data
{
    public class BannerRepository : IBannerRepository
    {
        private readonly IMongoCollection<Banner> _bannerCollection;

        public BannerRepository(IOptions<MongoDbSettings> mongoDbSettings)
        {
            var client = new MongoClient(mongoDbSettings.Value.ConnectionString);
            var db = client.GetDatabase(mongoDbSettings.Value.DatabaseName);

            _bannerCollection = db.GetCollection<Banner>(mongoDbSettings.Value.CollectionName);
        }

        public async Task<IEnumerable<Banner>> FindAll()
        {
            try
            {
                var banners = await _bannerCollection.FindAsync(_ => true);

                return await banners.ToListAsync();
            }
            catch (Exception e)
            {
                //Log with ai or something else to get telemetry about the error.
                throw;
            }
        }

        public async Task<Banner> GetById(string id)
        {
            try
            {
                var banner = await _bannerCollection.FindAsync(x => x.Id == id);

                return banner.FirstOrDefault();
            }
            catch (Exception e)
            {
                //Log with ai or something else to get telemetry about the error.
                throw;
            }

        }

        public async Task<Banner> Insert(Banner banner)
        {
            try
            {
                await _bannerCollection.InsertOneAsync(banner);

                return banner;
            }
            catch (Exception e)
            {
                //Log with ai or something else to get telemetry about the error.
                throw;
            }

        }

        public async Task<bool> Delete(string id)
        {
            try
            {
                var result = await _bannerCollection.DeleteOneAsync(x => x.Id == id);

                return result.DeletedCount == 1;
            }
            catch (Exception e)
            {
                //Log with ai or something else to get telemetry about the error.
                throw;
            }

        }

        public async Task<Banner> Update(Banner banner)
        {
            try
            {
                var filter = Builders<Banner>.Filter.Eq(x => x.Id, banner.Id);
                var update = Builders<Banner>.Update
                    .Set(x => x.Html, banner.Html)
                    .Set(x => x.Modified, DateTime.Now);

                var result = await _bannerCollection.UpdateOneAsync(filter, update);

                if (!result.IsAcknowledged) return null;
                if (result.MatchedCount != 1) return null;
                if (result.ModifiedCount != 1) return null;

                var updatedBanner = await _bannerCollection.FindAsync(x => x.Id == banner.Id);

                return updatedBanner.FirstOrDefault();
            }
            catch (Exception e)
            {
                //Log with ai or something else to get telemetry about the error.

                throw;
            }

        }
    }
}