﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using BannerFlowApi.ResourceModels;
using BannerFlowApi.Test.Host;
using BannerFlowApi.Test.Seed;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace BannerFlowApi.Test.Integration
{
    public class BannerApiTest : IDisposable
    {
        private readonly DbSeeder _dbSeeder;
        private readonly HttpClient _client;
        private readonly ITestOutputHelper _output;
        

        public BannerApiTest(ITestOutputHelper output)
        {
            var host = new TestHost();
            _client = host.HttpClient;
            _dbSeeder = host.Seed;
            _output = output;
        }

        [Theory]
        [InlineData("5b1d3c203e10bd6604d6c8d6")]
        public async void Should_Retrieve_One_Banner_By_Given_Id_And_Get_Ok_Response(string id)
        {
            //Arrange
            //...

            //Act
            var response = await _client.GetAsync($"http://localhost:5000/api/banner/{id}");
            var banner = JsonConvert.DeserializeObject<GetBannerResourceModel>(await response.Content.ReadAsStringAsync());

            //Assert
            Assert.True(response.StatusCode == HttpStatusCode.OK);
            Assert.True(banner != null);
        }

        [Fact]
        public async void Should_Retrieve_All_Banners_And_Get_Ok_Response()
        {
            //Arrange
            //...

            //Act
            var response = await _client.GetAsync($"http://localhost:5000/api/banner");
            var responseAsString = await response.Content.ReadAsStringAsync();
            var banners = JsonConvert.DeserializeObject<List<GetBannerResourceModel>>(responseAsString);

            //Assert
            _output.WriteLine($"{response.StatusCode} - {responseAsString}");
            Assert.True(response.StatusCode == HttpStatusCode.OK);
            Assert.True(banners.Any());
        }

        [Fact]
        public async void Should_Post_Banner_And_Get_Banner_From_Route_And_Created_Response()
        {
            //Arrange
            var model = new CreateBannerResourceModel
            {
                Html = "<div class=\"w3-content\"><img class=\"mySlides\" src=\"https://image.ibb.co/ewBqNF/custom_html_banner1.jpg\" style=\"width:100%\"><img class=\"mySlides\" src=\"https://image.ibb.co/fzVmev/custom_html_banner3.jpg\" alt=\"custom_html_banner3\" style=\"width:100%\"><img class=\"mySlides\" src=\"https://image.ibb.co/e4rL6a/custom_html_banner4.jpg\" alt=\"custom_html_banner4\" style=\"width:100%\"><img class=\"mySlides\" src=\"https://image.ibb.co/cFrgev/custom_html_banner2.jpg\" style=\"width:100%\"></div>"
            };

            //Act
            var jsonContent = JsonConvert.SerializeObject(model);

            var response = await _client.PostAsync("http://localhost:5000/api/banner",
                new StringContent(jsonContent, Encoding.UTF8, "application/json"));

            //Assert
            _output.WriteLine($"{response.StatusCode} - {await response.Content.ReadAsStringAsync()}");
            Assert.True(response.StatusCode == HttpStatusCode.Created);

        }

        [Fact]
        public async void Should_Post_Banner_And_Edit_Banner_With_A_Put_And_Get_Updated_Banner_As_Response()
        {
            //Arrange
            var model = new CreateBannerResourceModel
            {
                Html = "<div class=\"w3-content\"><img class=\"mySlides\" src=\"https://image.ibb.co/ewBqNF/custom_html_banner1.jpg\" style=\"width:100%\"><img class=\"mySlides\" src=\"https://image.ibb.co/fzVmev/custom_html_banner3.jpg\" alt=\"custom_html_banner3\" style=\"width:100%\"><img class=\"mySlides\" src=\"https://image.ibb.co/e4rL6a/custom_html_banner4.jpg\" alt=\"custom_html_banner4\" style=\"width:100%\"><img class=\"mySlides\" src=\"https://image.ibb.co/cFrgev/custom_html_banner2.jpg\" style=\"width:100%\"></div>"
            };

            var editModel = new EditBannerResourceModel
            {
                Html = "<p>Hello World!</p>"
            };

            //Act
            var postContent = JsonConvert.SerializeObject(model);

            var postResponse = await _client.PostAsync("http://localhost:5000/api/banner",
                new StringContent(postContent, Encoding.UTF8, "application/json"));
            var postResponseAsString = await postResponse.Content.ReadAsStringAsync();
            _output.WriteLine($"{postResponse.StatusCode} - {postResponseAsString}");

            var id = JsonConvert.DeserializeObject<GetBannerResourceModel>(postResponseAsString).Id;

            var putContent = JsonConvert.SerializeObject(editModel);
            var putResponse = await _client.PutAsync($"http://localhost:5000/api/banner/{id}",
                new StringContent(putContent, Encoding.UTF8, "application/json"));
            var putResponseAsString = await putResponse.Content.ReadAsStringAsync();
            var getBannerResourceModel = JsonConvert.DeserializeObject<GetBannerResourceModel>(putResponseAsString);

            _output.WriteLine($"{putResponse.StatusCode} - {putResponseAsString}");

            //Assert
            Assert.True(putResponse.StatusCode == HttpStatusCode.OK);
            Assert.Equal(getBannerResourceModel.Html, editModel.Html);
        }

        [Theory]
        [InlineData("5b1d3ba13e10bd6604d6c8d4")]
        public async void Should_Delete_Banner_And_Return_Response_No_Content(string id)
        {
            //Arrange
            //...

            //Act

            var deleteResponse = await _client.DeleteAsync($"http://localhost:5000/api/banner/{id}");
            var deleteResponseAsString = await deleteResponse.Content.ReadAsStringAsync();

            _output.WriteLine($"{deleteResponse.StatusCode} - {deleteResponseAsString}");
            Assert.True(deleteResponse.StatusCode == HttpStatusCode.NoContent);

            var getResponse = await _client.GetAsync($"http://localhost:5000/api/banner/{id}");
            var getResponseAsString = await getResponse.Content.ReadAsStringAsync();

            _output.WriteLine($"{getResponse.StatusCode} - {getResponseAsString}");
            Assert.True(getResponse.StatusCode == HttpStatusCode.NotFound);
        }

        public void Dispose()
        {
            _client?.Dispose();
            _dbSeeder?.UnSeed();
        }
    }
}