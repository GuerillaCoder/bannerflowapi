﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BannerFlowApi.Data;
using BannerFlowApi.DomainModels;

namespace BannerFlowApi.Test.Fakes
{
    public class FakeBannerRepository : IBannerRepository
    {
        private readonly List<Banner> _bannerCollection;

        public FakeBannerRepository()
        {
            _bannerCollection = new List<Banner>
            {
                new Banner("5b1d3ba13e10bd6604d6c8d4", "<div></div>", DateTime.Now, null),
                new Banner("5b1d3c203e10bd6604d6c8d6", "<div><p></p></div>", DateTime.Now.AddDays(-4), DateTime.Now.AddDays(-1))
            
            };
        }

        public async Task<Banner> GetById(string id)
        {
            return await Task.FromResult(_bannerCollection.FirstOrDefault(x => x.Id == id));
        }

        public async Task<Banner> Insert(Banner banner)
        {
            banner.Id = "okdsaosfg";
            _bannerCollection.Add(banner);

            return await Task.FromResult(banner);
        }

        public async Task<bool> Delete(string id)
        {
            var bannerToDelete = _bannerCollection.FirstOrDefault(x => x.Id == id);

            if (bannerToDelete == null) return await Task.FromResult(false);
            _bannerCollection.Remove(bannerToDelete);

            return await Task.FromResult(true);
        }

        public async Task<Banner> Update(Banner banner)
        {
            var bannerToReplace = _bannerCollection.FirstOrDefault(x => x.Id == banner.Id);
            var index = _bannerCollection.IndexOf(bannerToReplace);

            if (index == -1) return await Task.FromResult(default(Banner));

            banner.Modified = DateTime.Now;
            _bannerCollection[index] = banner;

            return await Task.FromResult(banner);

        }

        public async Task<IEnumerable<Banner>> FindAll()
        {
            return await Task.FromResult(_bannerCollection);
        }
    }
}