# BannerFlow Backend Api
Backend code test for bannerflow. All test are runnable. For integration testing tests are runnable using either the FakeRepository-implementation or the BannerReposiotry which uses a local MongoDb instance lestening on the default url.
To switch between the implementations use the constructor for TestHost that takes an enum. It defaults to 'HostServiceType.Fake' but when calling the ctor with 'HostServiceType.Database' it uses the BannerRepository.

When testing with MongoDb the bannerCollection will be seeded before tests are run and afterwards the collection will be disposed.