﻿using BannerFlowApi.DomainModels;
using BannerFlowApi.Helpers;

namespace BannerFlowApi.ResourceModels
{
    public class UpdateBannerResourceModel
    {
        [Html]
        public string Html { get; set; }

        public Banner ToBanner()
        {
            return new Banner
            {
                Html = Html
            };
        }
    }
}