﻿using BannerFlowApi.Helpers;

namespace BannerFlowApi.ResourceModels
{
    public class EditBannerResourceModel
    {
        [Html]
        public string Html { get; set; }
    }
}
