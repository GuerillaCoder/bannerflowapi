﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BannerFlowApi.DomainModels;

namespace BannerFlowApi.Data
{
    public interface IBannerRepository
    {
        Task<IEnumerable<Banner>> FindAll();
        Task<Banner> GetById(string id);
        Task<Banner> Insert(Banner banner);
        Task<bool> Delete(string id);
        Task<Banner> Update(Banner banner);
    }
}