﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BannerFlowApi.DomainModels
{
    public class Banner
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Html { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
       

        public Banner()
        {
            
        }

        public Banner(string id, string html, DateTime created, DateTime? modified)
        {
            Id = id;
            Html = html;
            Created = created;
            Modified = modified;
        }
    }
}