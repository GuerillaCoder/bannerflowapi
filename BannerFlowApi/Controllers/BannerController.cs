﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using BannerFlowApi.Data;
using BannerFlowApi.DomainModels;
using BannerFlowApi.Helpers;
using BannerFlowApi.ResourceModels;
using Microsoft.AspNetCore.Mvc;

namespace BannerFlowApi.Controllers
{
    [ValidateModel]
    [Route("api/[controller]")]
    public class BannerController : Controller
    {
        private readonly IBannerRepository _repo;

        public BannerController(IBannerRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var banners = (await _repo.FindAll()).ToList();

            if (!banners.Any())
            {
                return NotFound();
            }
            var model = banners.Select(GetBannerResourceModel.FromBanner).ToList();

            return Ok(model);
        }

        [HttpGet("{id}", Name = "GetBanner")]
        public async Task<IActionResult> Get(string id)
        {
            var banner = await _repo.GetById(id);

            if (banner is null) return NotFound();

            var model = new GetBannerResourceModel
            {
                Id = banner.Id,
                Created = banner.Created,
                Html = banner.Html,
                Modified = banner.Modified
            };

            return Ok(model);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateBannerResourceModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var banner = new Banner
            {
                Html = model.Html,
                Created = DateTime.Now
            };

            var response = await _repo.Insert(banner);

            return CreatedAtRoute("GetBanner", new { id = response.Id }, response);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, [FromBody]UpdateBannerResourceModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var banner = model.ToBanner();
            banner.Id = id;

            var response = await _repo.Update(banner);

            if (response != null)
            {
                return Ok(response);
            }

            return BadRequest();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var isSuccsessfulDelete = await _repo.Delete(id);

            if (isSuccsessfulDelete)
            {
                return NoContent();
            }

            return BadRequest();
        }

        [HttpGet]
        [Route("html/{id}")]
        [Produces("text/html")]
        public async Task<ContentResult> GetHtml(string id)
        {
            var banner = await _repo.GetById(id);

            if (banner is null)
                return new ContentResult
                {
                    StatusCode = (int) HttpStatusCode.NoContent,
                };

            return new ContentResult
            {
                StatusCode = (int)HttpStatusCode.OK,
                ContentType = "text/html",
                Content = banner.Html
            };
        }
    }
}