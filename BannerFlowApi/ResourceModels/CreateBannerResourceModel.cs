﻿using BannerFlowApi.Helpers;
using Newtonsoft.Json;

namespace BannerFlowApi.ResourceModels
{
    public class CreateBannerResourceModel
    {
        [Html]
        public string Html { get; set; }
    }
}