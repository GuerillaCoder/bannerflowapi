﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using HtmlAgilityPack;

namespace BannerFlowApi.Helpers
{
    public class HtmlAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var html = value.ToString();
            return !IsValidHtml(html) ? new ValidationResult("The given html is invalid.") : ValidationResult.Success;
        }

        public bool IsValidHtml(string html)
        {
            var doc = new HtmlDocument();

            doc.LoadHtml(html);
            
            return !doc.ParseErrors.Any();
        }
    }
}