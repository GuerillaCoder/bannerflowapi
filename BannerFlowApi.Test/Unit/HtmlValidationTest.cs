﻿using BannerFlowApi.Helpers;
using Xunit;

namespace BannerFlowApi.Test.Unit
{
    public class HtmlValidationTest
    {
        [Fact]
        public void Should_Validate_Html_As_Valid_Given_Valid_Html()
        {
            const string html = "<html><head></head></html>";
            var htmlHelper = new HtmlAttribute();

            var isValidHtml = htmlHelper.IsValidHtml(html);

            Assert.True(isValidHtml);

        }

        [Fact]
        public void Should_Invalidate_Html_Given_Html_Tag_Is_Invalid()
        {
            //Missing one bracket in first html-tag
            const string html = "<html<head></head></html>";
            var htmlHelper = new HtmlAttribute();

            var isValidHtml = htmlHelper.IsValidHtml(html);

            Assert.False(isValidHtml);
        }

     
    }
}